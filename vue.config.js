module.exports = {
  publicPath: process.env.VUE_APP_URL_PATH,

  configureWebpack: {
    output: {
      libraryExport: 'default'
    }
  },
  css: {
    extract : false
  },

  pluginOptions: {
    i18n: {
      locale: 'fr',
      fallbackLocale: 'en',
      enableInSFC: true
    }
  },

  transpileDependencies: [
    'vuetify'
  ]
}
