var config = {
    APP_NAME : process.env.VUE_APP_APP_NAME,
    END_POINT: process.env.VUE_APP_JAMA_END_POINT,
    API_KEY: process.env.VUE_APP_JAMA_API_KEY,
    DEFAULT_PROJECT_ID: process.env.VUE_APP_JAMA_DEFAULT_PROJECT_ID,
    DEFAULT_COLLECTION_ID: process.env.VUE_APP_JAMA_DEFAULT_COLLECTION_ID,
    SUPPORTED_FORMATS : process.env.VUE_APP_JAMA_SUPPORTED_FORMATS.split(','),
    APP_TITLE: process.env.VUE_APP_TITLE,
    LOGOUT_PATH: process.env.VUE_APP_LOGOUT_PATH,
    PICKER_MODE: process.env.VUE_APP_PICKER_MODE,
    THESAURUS_URL: process.env.VUE_APP_THESAURUS_URL,
    THESAURUS_LANG: process.env.VUE_APP_THESAURUS_LANG,
    THESAURUS_ID: process.env.VUE_APP_THESAURUS_ID

}

module.exports = config
