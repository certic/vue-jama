import Vue from 'vue'
import Vuex from 'vuex'
import EventBus from "../events/event-bus";

let JamaRPClient = null;

Vue.use(Vuex)


const DESC_SORT = 'desc';
const ASC_SORT = 'asc';


function sortOptionsAsString(sortOptions) {
    try {
        let prefix = sortOptions.order === DESC_SORT ? '-' : '';
        return prefix + sortOptions.type;
    } catch (e) {
        return 'title'
    }
}

/**
 * collectionsTree's item finder
 * @param id
 * @param items
 * @returns {*}
 */
function findItem(id, items = null) {
    return items.reduce((acc, item) => {
        if (acc) {
            return acc;
        }
        if (item.id === id) {
            return item;
        }
        if (item.children) {
            return findItem(id, item.children);
        }
        return acc;
    }, null);
}

import localStorageStore from './localStorageStore';

export default new Vuex.Store({
    state: {
        appTitle: '',
        appSubtitle: null,
        projects: [],
        resources: [],
        collections: [],
        collectionsTree: [],
        projectProperties: [],
        allSelected: false,
        selectedCollections: [],
        selectedResources: [],
        permissions: [],
        projectsUserPermissions: [],
        currentProjectId: null,
        filter: [],
        public_access_filter: null,
        currentRootCollectionId: null,
        currentCollection: null,
        currentResource: null,
        currentStats: null,
        pickerMode: false,
        acceptedFormats: [],
        preferences: {},//{...DEFAULT_PREFERENCES}
        showHiddenCollections: true,
        editableTags: true,
        sortOptions: {
            order: ASC_SORT,
            type: 'title'
        },
        multiSelectActionAddons: [],
        uploads : [],

    },

    getters: {
        appTitle: state => state.appTitle,
        appSubtitle: state => state.appSubtitle,
        projects: state => state.projects,
        resources: state => state.resources,
        collections: state => state.collections,
        collectionsTree: state => state.collectionsTree,
        selectedResources: state => state.selectedResources,
        selectedCollections: state => state.selectedCollections,
        allSelected: state => state.allSelected,
        permissions: state => state.permissions,
        projectsUserPermissions: state => state.projectsUserPermissions,
        currentCollection: state => state.currentCollection,
        currentResource: state => state.currentResource,
        currentProjectId: state => state.currentProjectId,
        projectProperties: state => state.projectProperties,
        currentRootCollectionId: state => state.currentRootCollectionId,
        isPickerMode: state => state.pickerMode,
        preferences: state => state.preferences,
        acceptedFormats: state => state.acceptedFormats,
        showHiddenCollections: state => state.showHiddenCollections,
        sortOptions: state => state.sortOptions,
        currentStats: state => state.currentStats,
        filter: state => state.filter,
        public_access_filter: state => state.public_access_filter,
        multiSelectActionAddons: state => state.multiSelectActionAddons,
        uploads : state => state.uploads,
        editableTags : state => state.editableTags
    },
    mutations: {

        setAppTitle: (state, t) => {
            state.appTitle = t;
        },

        setAppSubtitle: (state, st) => {
            state.appSubtitle = st;
        },

        setJamaAPIClient: (state, client) => {
            JamaRPClient = client;
        },

        setProjects: (state, projects) => (
            state.projects = projects
        ),

        setResources: (state, resources) => {
            state.resources = resources;
        },

        setCollections: (state, collections) => {
            state.collections = collections;
        },

        setCollectionsTree: (state, tree) => {
            state.collectionsTree = tree
        },

        setAllSelected: (state, bool) => {
            state.allSelected = bool;
        },

        setSelectedResources: (state, resources) => {
            state.selectedResources = resources;
        },

        setSelectedCollections: (state, collections) => {
            state.selectedCollections = collections;
        },

        setProjectProperties: (state, properties) => {
            state.projectProperties = properties;
        },

        setMultiSelectActionAddons: (state, actionAddons) => {
            state.multiSelectActionAddons = actionAddons
        },

        setEditableTags: (state, editable) => {
            state.editableTags = editable
        },

        addCollection: (state, collection) => {
            state.collections = [...state.collections, collection];
        },

        addSelectedCollection: (state, collection) => {
            state.selectedCollections = [...state.selectedCollections, collection];
        },

        addSelectedResource: (state, resource) => {
            state.selectedResources = [...state.selectedResources, resource];
        },

        deleteCollection: (state, collectionId) => {
            state.collections = state.collections.filter(c => c.id !== collectionId);
        },

        removeSelectedCollection: (state, collectionId) => {
            state.selectedCollections = state.selectedCollections.filter(c => c.id !== collectionId);
        },

        removeSelectedResource: (state, resourceId) => {
            state.selectedResources = state.selectedResources.filter(r => r.id !== resourceId);
        },

        setCurrentCollection: (state, collection) => (
            state.currentCollection = collection && collection.id ? {...collection} : null
        ),

        setProjectsUserPermissions: (state, permissions) => (
            state.projectsUserPermissions = permissions
        ),

        setPermissions: (state, permissions) => (
            state.permissions = permissions
        ),

        setCurrentProjectId: (state, projectId) => {
            state.currentProjectId = projectId ? Number(projectId) : null;
        },
        setCurrentRootCollectionId: (state, collectionId) => {
            state.currentRootCollectionId = collectionId ? Number(collectionId) : null;
        },

        setCurrentResource: (state, resource) => {
            state.currentResource = resource;
            if (state.currentResource) {
                let index = state.resources.findIndex(r => r.id === state.currentResource.id);
                state.resources[index] = state.currentResource;
                state.resources = [...state.resources];
            }
        },

        emptyResources : (state) => {
          state.resources = [];
        },

        setCurrentStats: (state, stats) => (
            state.currentStats = stats
        ),

        deleteResource: (state, resourceId) => {
            state.resources = state.resources.filter(r => r.id !== resourceId);
        },

        setPickerMode: (state, bool) => {
            state.pickerMode = bool
        },

        setShowHiddenCollections: (state, bool) => {
            state.showHiddenCollections = bool
        },

        updatePreferences: (state, preferences) => (
            state.preferences = {...state.preferences, preferences}
        ),

        setAcceptedFormats: (state, acceptedFormats) => (
            state.acceptedFormats = acceptedFormats
        ),

        setSortOptions: (state, sortOptions) => (
            state.sortOptions = sortOptions
        ),

        setFilter: (state, filter) => (
            state.filter = filter
        ),

        setPublicAccessFilter: (state, filter) => {
            state.public_access_filter = filter;
        }
    },
    actions: {

        async fetchProjects({commit}) {
            let projects = await JamaRPClient.projectsUserPermissions();
            commit('setProjects', projects);
        },

        async fetchPermissions({commit}) {
            let permissions = await JamaRPClient.listPermissions();
            commit('setPermissions', permissions);
            return permissions
        },

        async fetchUserPermissions({commit}) {
            let permissions = await JamaRPClient.projectsUserPermissions();
            commit('setProjectsUserPermissions', permissions);
        },

        async fetchItems(context,
                         {
                             projectId = null,
                             includeMetas = false,
                             collectionId = null,
                             limitFrom = 0,
                             limitTo = 2000
                         }
        ) {
            let res = await JamaRPClient.projectItems(
                context.getters.filter,
                projectId,
                includeMetas,
                collectionId,
                limitFrom,
                limitTo,
                sortOptionsAsString(context.getters.sortOptions),
                context.getters.public_access_filter
                );

            let collections = res.collections;
            let resources = res.resources;
            //filters "hidden collections" -> title starting with "."
            if (!context.getters.showHiddenCollections) {
                collections = collections.filter(c => {
                    return !c.title.startsWith('.')
                });
            }

            context.commit('setCollections', collections);
            context.commit('setResources', resources);
            return res;

        },

        async fetchCollections(context,
                               {
                                   parentId = null,
                                   recursive = false,
                                   limitFrom = 0,
                                   limitTo = 2000,
                                   flatList = false,
                                   onlyPublished = false,
                                   onlyDeletedItems = false
                               }) {
            let collections = await JamaRPClient.collections(
                parentId,
                recursive,
                limitFrom,
                limitTo,
                flatList,
                onlyPublished,
                sortOptionsAsString(context.getters.sortOptions),
                onlyDeletedItems
            );

            //filters "hidden collections" -> title starting with "."
            if (!context.getters.showHiddenCollections) {
                collections = collections.filter(c => {
                    return !c.title.startsWith('.')
                });
            }

            context.commit('setCollections', collections);
        },

        async fetchCollection({commit}, id) {
            let collection = await JamaRPClient.collection(id);
            commit('setCurrentCollection', collection);
        },

        async fetchCollectionTree(context, id) {
            let collections = await JamaRPClient.collections(id);
            collections.forEach((c) => {
                if (c.children_count > 0)
                    c.children = []
            });
            if (!context.getters.showHiddenCollections) {
                collections = collections.filter(c => {
                    return !c.title.startsWith('.')
                });
            }
            let item = findItem(id, context.state.collectionsTree);
            if (!item) {
                context.commit('setCollectionsTree', collections);
            } else {
                if(collections.length > 0)
                    item.children = collections;
                else {
                    item.children = null;
                }
                context.commit('setCollectionsTree', [...context.state.collectionsTree]);
            }
        },

        async fetchResources(context, {collectionId = null, includeMetas = false, limitFrom = 0, limitTo = 2000}) {
            let resources = await JamaRPClient.resources(
                collectionId,
                includeMetas,
                limitFrom,
                limitTo,
                sortOptionsAsString(context.getters.sortOptions)
            );
            context.commit('setResources', resources);
        },

        async fetchResource({commit}, resourceId) {
            let resource = await JamaRPClient.resource(resourceId);
            commit('setCurrentResource', resource);
        },

        async deleteResource(context, resourceId) {
            let response = await JamaRPClient.deleteResource(resourceId)
            if (response) {
                context.commit('deleteResource', resourceId);
            }
            return response;
        },

        async renameResource(context, {resourceId, name, current = true}) {
            await JamaRPClient.renameResource(resourceId, name);
            if (current) {
                context.commit('setCurrentResource', {...context.getters.currentResource, title: name});
            }
        },

        async deleteCollection(context, {collectionId, recursive}) {
            let rid = context.state.currentCollection.id === collectionId ?  context.state.currentCollection.parent : context.state.currentCollection.id
            let res = await JamaRPClient.deleteCollection(collectionId, recursive);
            context.commit('deleteCollection', collectionId);
            await context.dispatch('fetchCollectionTree', rid);
            return res;
        },

        async removeSelection(context, {collectionId, selection}){
            return JamaRPClient.removeSelection(collectionId, selection);
        },

        async moveSelection(context, {fromCollectionId, toCollectionId, selection}){
            return JamaRPClient.moveSelection(fromCollectionId, selection, toCollectionId);
        },

        async renameCollection(context, {collectionId, name}) {
            let r = await JamaRPClient.renameCollection(collectionId, name);

            if(r && collectionId === context.state.currentCollection.id){
                let c = {...context.state.currentCollection, title : name};
                context.commit('setCurrentCollection', c);
                let tc = findItem(collectionId, context.state.collectionsTree);
                tc.title = name;
            }
            return r;
        },

        async addCollection(context, {title, parentId}) {
            let newCollection = await JamaRPClient.addCollection(title, parentId);
            if (newCollection) {
                context.commit('addCollection', newCollection);
                await context.dispatch('fetchCollectionTree', parentId);
            }
            return newCollection
        },

        async removeResourceFromCollection(context, {resourceId, collectionId}) {
            let response = await JamaRPClient.removeResourceFromCollection(resourceId, collectionId)
            if (response) {
                context.commit('deleteResource', resourceId);
            }
            return response;
        },

        async fetchProjectProperties(context) {
            let response = await JamaRPClient.projectProperties(context.getters.currentProjectId);
            console.log(response);
            if (response) {
                context.commit('setProjectProperties', response);
            }
            return response;
        },

        async updateProjectProperty(context, {propertyKey, propertyValue}) {
            let response = await JamaRPClient.setProjectProperty(context.getters.currentProjectId, propertyKey, propertyValue);
            if (response) {
                let props = [...context.getters.projectProperties];
                const index = props.findIndex((o => o.key === propertyKey));
                props[index].value = propertyValue;
                context.commit('setProjectProperties', props);
            }
            return response;
        },

        async deleteProjectProperty(context, propertyKey) {
            let response = await JamaRPClient.deleteProjectProperty(context.getters.currentProjectId, propertyKey);
            if (response) {
                let props = context.getters.projectProperties.filter((elt) => elt.key !== propertyKey);
                context.commit('setProjectProperties', props);
            }
            return response;
        },


        /**
         *
         * @param context
         * @param files
         * @param path
         * @returns {boolean}
         */
        async manageFilesUpload(context, {files, path, projectId}) {
            let uploadResults = await JamaRPClient.uploadFiles(files, projectId, path, context.getters.acceptedFormats);
            if(!uploadResults[0])
                return false;
            //let errorMessages = [];
            for (let i = 0; i < uploadResults.length; i++) {
                if (uploadResults[i].already_uploaded) {
                    try {
                        await JamaRPClient.addResourceToCollection(uploadResults[i].id, context.getters.currentCollection.id);
                    } catch (e) {
                        console.log(e);
                        console.log('cannot add ' + uploadResults[i].id)
                    }
                }/* else {
                    if (uploadResults[i].chunk_errors || uploadResults[i].status === 'bad_format') {
                        errorMessages.push(uploadResults[i].message);
                    }
                }*/
            }
            let onError = uploadResults.filter((ur) => !ur.ok);
            if(onError.length > 0 )
                EventBus.$emit('JAMA_NOTIFICATION', {
                    type: 'error',
                    timeout: 3000,
                    message: `Upload terminé avec ${onError.length} fichier(s) en erreur : `
                        + onError.map((item) => item.name).join(', '),
                    visible: true
                })
            else
                    EventBus.$emit('JAMA_NOTIFICATION', {
                        type: 'success',
                        timeout: 3000,
                        message: `Upload terminé avec succès : ${files.length} fichier(s)`,
                        visible: true
                    })

            return true;
        },

        /**
         *
         * @param context
         * @param  {BigInteger} from
         * @param  {BigInteger} to
         * @returns {Boolean}
         */
        async replaceResource(context, {from, to}) {
            return JamaRPClient.replaceFile(from, to);
        },

        /**
         *
         * @param context
         * @param item
         * @returns {Promise<void>}
         */
        async fetchStats(context) {
            if (context.getters.currentCollection) {
                const queryCollId = context.getters.currentCollection.id;
                JamaRPClient.collectionStats(context.getters.currentCollection.id).then((res) => {
                    if (queryCollId === context.getters.currentCollection.id)
                        context.commit('setCurrentStats', res)
                })
            } else {
                context.commit('setCurrentStats', await JamaRPClient.projectStats(context.getters.currentProjectId));
                context.commit('setCurrentRootCollectionId', context.getters.currentStats.project_root_collection_id);
            }
        }

    },
    modules: {
        localStorageStore: localStorageStore
    }
})
