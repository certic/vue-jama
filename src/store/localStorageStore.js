const MOSAIC = 0;
const FULLSCREEN = 1;
const LIST = 2;
const LOCAL_STORAGE_APP_ID = 'vue-jama';

const localStorageStore = {
    namespaced: true,
    state: () => ({
        view: localStorage.getItem(LOCAL_STORAGE_APP_ID + '.view')
            ? Number(localStorage.getItem(LOCAL_STORAGE_APP_ID + '.view'))
            : MOSAIC,

        treeDrawer: localStorage.getItem(LOCAL_STORAGE_APP_ID + '.treeDrawer')
            ? localStorage.getItem(LOCAL_STORAGE_APP_ID + '.treeDrawer') === 'true'
            : true,

    }),
    getters: {
        isFullscreenView: state => state.view === FULLSCREEN,
        isMosaicView: state => state.view === MOSAIC,
        isListView: state => state.view === LIST,
        view: state => state.view,
        treeDrawer: state => state.treeDrawer,
        uploads: state => state.uploads
    },
    mutations: {
        setView: (state, vm) => {
            state.view = vm
            localStorage.setItem(LOCAL_STORAGE_APP_ID + '.view', vm);
        },

        setTreeDrawer: (state, b) => {
            state.treeDrawer = b
            localStorage.setItem(LOCAL_STORAGE_APP_ID + '.treeDrawer', b);
        },

    },
    actions: {
        resetView(context) {
            context.commit('setView', MOSAIC)
        },

    }
}

export default localStorageStore