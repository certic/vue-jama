import Vue from 'vue'
import App from './App.vue'
import VueI18n from 'vue-i18n';
// import config from './config/config';
//import store from './store/JamaStore';
require('@mdi/font/css/materialdesignicons.min.css');
require('./assets/css/jama.scss');

 Vue.use(VueI18n);


const defaultLocale = 'fr';


let locale = navigator.language.split('-')[0];
const i18n = new VueI18n({
    fallbackLocale: defaultLocale,
    locale: locale});

new Vue({
    i18n,
    render: h => h(App)
}).$mount('#app')

