import JamaTagClient from "./JamaTagClient";

class JamaOpenthesoTagClient extends JamaTagClient{


    constructor(jamaClient, jamaProjectId, openthesoEndpoint, openthesoId, openthesoLang) {
        super(jamaClient, jamaProjectId);
        this.openthesoClient = new OpenthesoClient(openthesoEndpoint, openthesoId, openthesoLang);
    }

    async doQuery(term) {
        console.log("doing query")
        let jamaRes = await super.doQuery(term);
        let openThesoRes = await this.openthesoClient.doQuery(term);
        //filter : avoid duplicate entry (already added to jama are filtered from opentheso response)
        openThesoRes = openThesoRes.filter((item) => {
            return jamaRes.map((e) => {return e.ark}).indexOf(item.ark) < 0
        })
        return jamaRes.concat(openThesoRes);
    }
}

class OpenthesoClient{
    constructor(endpoint, thesaurusId, lang) {
        this.endpoint = endpoint;
        this.thesaurusId = thesaurusId;
        this.lang = lang
    }
    async doQuery(term) {

        if(term.length < 2)
            return []
        let response = await fetch(this.endpoint
            + "search?q="
            + term
            + "&lang="
            + this.lang
            + "&theso="
            + this.thesaurusId, {
            method: "GET",
            mode : 'cors',
            headers: {
                'Accept':'application/json'
            }
        });

        let json = await response.json();
        let entries = [];
        let self = this;
        Object.entries(json).forEach(([k, v]) => {
            let id = v['http://purl.org/dc/terms/identifier'][0].value;
            let href = k;
            let prefLabels =v['http://www.w3.org/2004/02/skos/core#prefLabel'].filter(function(e){
                return e.lang === self.lang;
            });

            entries.push({id: id, href: href, label: prefLabels[0].value, ark: href})
        })
        return entries;



    }

    getPublicURL(){
        return this.endpoint.replace('api/', '')
    }

    getEndPoint() {
        return this.endpoint;
    }

}



export {JamaOpenthesoTagClient, OpenthesoClient}


