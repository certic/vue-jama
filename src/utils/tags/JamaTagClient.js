
class JamaTagClient{


    constructor(jamaClient, projectId = null) {
        this.jamaClient = jamaClient;
        this.projectId = projectId;
    }

    async doQuery(term) {
        let res =  await this.getAllTags()
        res = res.filter((e) => e.label.toLowerCase().includes(term.toLowerCase()));
        return res;
    }

    setProjectId(projectId){
        this.projectId = projectId;
    }

    async getAllTags(){
        let res = await this.jamaClient.tags(this.projectId);
        return res;
    }
}


export default JamaTagClient;


