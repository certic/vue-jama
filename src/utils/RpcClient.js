class RpcClient {

    constructor(endPoint = '', apiKey ='', rpcMethodPrefix = '') {
        this.rpcMethodPrefix = rpcMethodPrefix;
        this.endPoint = endPoint;
        this.apiKey = apiKey;
        this.id = 0;
    }

    /**
     *
     * @param {String} method
     * @param {Array} params
     * @returns {Any}
     */
    async _rpc(method, params) {
        this.id++;
        let rpc_request = {
            id: this.id,
            method: this.rpcMethodPrefix + method,
            params: params
        };
        let body = JSON.stringify(rpc_request);
        let headers = new Headers();
        headers.set('X-Api-Key', this.apiKey);
        let response = await fetch(this.endPoint, {
            method: 'POST',
            headers: headers,
            body: body
        }).catch(() => {
            return false
        });
        if (response.ok) {
            let response_dict = await response.json();
            if (response_dict.error) {
                throw response_dict.error;
            }
            return response_dict.result;
        } else return false
    }


    /**
     * Add access to the RPC API for the given user name with the given API key.
     * A new user will be created if none is available with given user name.
     *
     * Requires superuser.
     *
     * @param {String} user_name
     * @param {String} api_key
     * @returns {Boolean}
     */
    async activateRpcAccess(user_name, api_key) {
        return this._rpc("activate_rpc_access", [user_name, api_key]);
    }
}

export default RpcClient