const FOLDERS_ONLY_MODE = 1;
const ALL_MODE = 2;
class DirectoryDropReader {

    constructor(item, mode = ALL_MODE) {
        this.files = []
        this.finishCallback = null;
        this.mode = mode;
    }

    traverse(item) {
        if(!item) {
            console.info('null dropped item ignored');
            return;
        }
        this.files = [];
        let self = this;
        if(item.isFile){
            return new Promise((resolve) => {
                item.file((f) => {
                    this.files.push(f);
                    resolve(this.files);
                });

            });
        }
        const reader = item.createReader();
        // Resolved when the entire directory is traversed
        return new Promise((resolve, reject) => {
            const iterationAttempts = [];
            function readEntries() {
                // According to the FileSystem API spec, readEntries() must be called until
                // it calls the callback with an empty array.  Seriously??
                reader.readEntries((entries) => {
                    if (!entries.length) {
                        // Done iterating this particular directory
                        resolve(Promise.all(iterationAttempts));
                    } else {
                        // Add a list of promises for each directory entry.  If the entry is itself
                        // a directory, then that promise won't resolve until it is fully traversed.
                        iterationAttempts.push(Promise.all(entries.map((ientry) => {
                            if (ientry.isFile) {
                                // DO SOMETHING WITH FILES
                                //return ientry;
                                ientry.file((f) => self.files.push(f))
                                return true;
                            }
                            // DO SOMETHING WITH DIRECTORIES
                            return self.traverse(ientry);
                        })));
                        // Try calling readEntries() again for the same dir, according to spec
                        readEntries();
                    }
                }, error => reject(error));
            }
            readEntries();
        });
    }

    read(){
        for (let i = 0; i < this.items.length; i++) {
            let item = this.items[i].webkitGetAsEntry();
            if (item) {
                this.readFiles(item);
            }
        }
    }

    readFiles(item){
        if (item.isDirectory) {
            let directoryReader = item.createReader();
            directoryReader.readEntries( (entries) => {
                entries.forEach( (entry) =>  {
                    this.readFiles(entry);
                });
            });
        }
        else{
            //this.files.push(item);
            item.file((f)=>this.files.push(f))
        }
    }

    getFiles(){
        return this.files;
    }

    // read(cb) {
    //     this.finishCallback = cb;
    //     //var entry = this.items[0].webkitGetAsEntry();
    //     for (let i = 0; i < this.nbItems; i++) {
    //         this.readItem(this.items[i].webkitGetAsEntry(), true);
    //     }
    // }
    //
    // readItem(item, isRoot = false) {
    //     if(!item)
    //         return;
    //     if (item.isDirectory) {
    //         let elemReader = item.createReader();
    //         let isLastDir = true;
    //         let dirRead =  () => {
    //             elemReader.readEntries( (subelems) => {
    //                 if (!subelems.length) {
    //                     this.finisher(isLastDir);
    //                 } else {
    //                     for (let i = 0; i < subelems.length; i++) {
    //                         this.readItem(subelems[i]);
    //                         isLastDir = isLastDir && subelems[i].isFile;
    //                     }
    //                     dirRead();
    //                 }
    //             });
    //         };
    //         dirRead();
    //     }
    //     if (item.isFile) {
    //         item.file( (efile) => {
    //             this.droppedFiles.push(efile);
    //             this.finisher(isRoot);
    //         });
    //     }
    // }
    //
    // finisher(isFull) {
    //     if(isFull && this.finishCallback)
    //         this.finishCallback();
    //
    // }

    getDroppedFiles(){
        return this.droppedFiles;
    }

    /**
     *
     * @param DataTransferItemList items
     */
    static areFoldersOnly(items){
        for(let i = 0; i < items.length; i++) {
            let item = items[i].webkitGetAsEntry();
            if(!item.isDirectory){
                return false;
            }
        }
        return true;
    }
}

export {DirectoryDropReader, FOLDERS_ONLY_MODE, ALL_MODE}
