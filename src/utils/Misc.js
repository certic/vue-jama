export default class Misc {

    static getResourceType(resource) {
        try {
            if (resource.type === 'application/pdf')
                return 'Image';
            else return (resource.type.split('/')[0]).replace(/^\w/, (c) => c.toUpperCase());
        } catch (e) {
            return null
        }
    }

    static getMetaValue(resource, setTitle, metaTitle) {
        let meta = resource.metas.find(
            (m) => m.meta.set_title.toLowerCase() === setTitle.toLowerCase()
                && m.meta.title.toLowerCase() === metaTitle.toLowerCase()
        )
        return meta ? meta.value : null
    }

    static makeid(length) {
        var result = '';
        var characters = 'abcdef0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    //add zeroes as prefix :  lpad(42,2) => 0042
    static lpad (value, padding) {
        const zeroes = new Array(padding + 1).join("0");
        return (zeroes + value).slice(-padding);
    }

}