import JamacRpcClient from './JamaRpcClient';
import Misc from "./Misc";


const CHUNK_SIZE = 1024 * 1024;
const CHUNK_MAX_RETRY = 3;
const CHUNK_RETRY_WAIT = 2000;

class ChunksUploader {

    constructor(file, endpoint, api_key, id, chunk_size = CHUNK_SIZE) {
        this.file = file;
        this.endpoint = endpoint;
        this.api_key = api_key;
        this.chunk_size = chunk_size;
        this.chunk_statuses = [];
        this.nb_chunks = Math.ceil(this.file.size / this.chunk_size);
        for (let i = 0; i < this.nb_chunks; i++) {
            this.chunk_statuses[i] = {n: i, done: false};
        }
        this.chunk_indexes = [];
        let byteIndex = 0
        for (let i = 0; i < this.nb_chunks; i++) {
            let byteEnd = Math.ceil((this.file.size / this.nb_chunks) * (i + 1));
            this.chunk_indexes.push({start: byteIndex, end: byteEnd})
            byteIndex += (byteEnd - byteIndex);
        }
        this.id = id;
    }


    fixedEncodeURIComponent(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    }

    /**
     * @param {Integer} chunk_num - chunk number
     * @param {String} path - collection path
     *  @param {BigInteger} projectId - project id
     * @return {Promise} Promise object of XHR
     */
    uploadChunk(chunkNum, path, projectId) {
        let data = this.file.slice(this.chunk_indexes[chunkNum].start, this.chunk_indexes[chunkNum].end);
        let options =
            {
                //url: this.endpoint + 'upload/partial/',
                method: 'POST',
                body: data,
                headers: {
                    "X-Api-Key": this.api_key,
                    "X-file-chunk": (chunkNum + 1) + "/" + this.nb_chunks,
                    "X-file-hash": this.id,
                    "X-file-name": window.btoa(unescape(this.fixedEncodeURIComponent(this.file.name))),
                    "X-Project": projectId,
                    //"X-debug-file-name" : this.file.name
                }
            }
        if (path)
            options.headers['X-origin-dir'] = path
        return fetch(this.endpoint + 'upload/partial/', options)
    }


    getId() {
        return this.id;
    }

    getFile() {
        return this.file;
    }


    getNbChunks() {
        return this.nb_chunks;
    }

}


const upload_events = {
    start_upload_event: 'start_upload',
    end_upload_event: 'end_upload',
    start_file_upload_event: 'start_file_upload',
    end_file_upload_event: 'end_file_upload',
    resume_upload_event: 'resume_upload',
    chunk_uploaded_event: 'chunk_uploaded'
}


class JamaClient extends JamacRpcClient {

    constructor(endPoint = '', apiKey = '', rpcMethodPrefix = '') {
        super(endPoint, apiKey, rpcMethodPrefix);
        this.listeners = [];
        //this.fileChunksUploader = null;
    }

    updateAPI(endPoint, apiKey) {
        this.endPoint = endPoint;
        this.apiKey = apiKey;
    }


    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async uploadChunk(fileChunksUploader, chunkNum, uploadId, path, projectId, attempts = 0) {
        console.info("Will upload chunk " + chunkNum + '/' + fileChunksUploader.getNbChunks())
        try {
            let response = await fileChunksUploader.uploadChunk(chunkNum, path, projectId);
            this.dispatchEvent(new CustomEvent(upload_events.chunk_uploaded_event, {
                    'detail': {
                        id: fileChunksUploader.getId(),
                        name: fileChunksUploader.getFile().name,
                        chunk: chunkNum,
                        nb_chunks: fileChunksUploader.getNbChunks(),
                        result: response
                    }
                })
            );
            if (!response.ok) {
                if (attempts < CHUNK_MAX_RETRY) {
                    console.info("chunk NOT OK (RETRY): " + (chunkNum + 1) + "/" + fileChunksUploader.getNbChunks());
                    await this.sleep(CHUNK_RETRY_WAIT);
                    return this.uploadChunk(fileChunksUploader, chunkNum, uploadId, path, projectId, attempts + 1)
                } else {
                    console.info("chunk NOT OK : " + (chunkNum + 1) + "/" + fileChunksUploader.getNbChunks());
                    return {
                        ok: false, hash: fileChunksUploader.getId(), status: 'incomplete'
                    }
                }
            } else {
                console.info("chunk ok : " + (chunkNum + 1) + "/" + fileChunksUploader.getNbChunks() + ' - ' + fileChunksUploader.getFile().name);
                return response;
            }
        } catch (err) {
            if (attempts < CHUNK_MAX_RETRY) {
                console.info("chunk NOT OK (RETRY): " + (chunkNum + 1) + "/" + fileChunksUploader.getNbChunks());
                await this.sleep(CHUNK_RETRY_WAIT);
                return this.uploadChunk(fileChunksUploader, chunkNum, uploadId, path, projectId, attempts + 1)
            } else {
                console.error("chunk NOT OK : " + (chunkNum + 1) + "/" + fileChunksUploader.getNbChunks());
                return {ok: false, hash: fileChunksUploader.getId(), status: 'incomplete'}
            }
        }

    }

    /**
     * @param file
     * @param uploadInfos
     * {
     *     hash : 1234,
     *     name: 'my_file_name',
     *     path: 'my_file_path/',
     *     nb_chunks: X,
     *     available_chunks: [...]
     * }
     */
    async resumeUpload(file, uploadInfos) {
        let result = await this.uploadOrResumeFile(file, uploadInfos.path, uploadInfos.project, uploadInfos);
        let uploadResult = this.manageUploadFileResult(file, result);
        this.dispatchEvent(new CustomEvent(upload_events.end_upload_event, {detail: [uploadResult]}))
    }


    async uploadOrResumeFile(file, path, projectId, uploadInfos) {
        uploadInfos = uploadInfos || {};
        let resourceId = uploadInfos ? uploadInfos.resourceId : null;
        let uploadId = null;
        let fileChunksUploader = null;
        if (uploadInfos.id) {//resume file upload
            fileChunksUploader = new ChunksUploader(file, this.endPoint, this.apiKey, uploadInfos.id);
            uploadId = uploadInfos.id;
            this.dispatchEvent(new CustomEvent(upload_events.resume_upload_event), {detail: {id: uploadId}});
        } else {//start file upload - need a uniqid
            uploadId = Misc.makeid(64);
            fileChunksUploader = new ChunksUploader(file, this.endPoint, this.apiKey, uploadId);
            this.dispatchEvent(new CustomEvent(upload_events.start_file_upload_event, {
                    'detail': {
                        id: uploadId,
                        name: file.name,
                        path: path,
                        project: projectId,
                        nb_chunks: fileChunksUploader.getNbChunks()
                    }
                })
            );
        }

        const nbChunks = fileChunksUploader.getNbChunks();
        for (let i = 0; i < nbChunks; i++) {
            if (!uploadInfos.available_chunks || uploadInfos.available_chunks.indexOf(
                //if nb chunks = 42 then first chunk = '42-01.part', if nb chunks = 142 then first chunk = '142-001.part'
                nbChunks
                + '-'
                + Misc.lpad(i + 1, nbChunks.toString().length) + '.part') < 0) {
                let response = await this.uploadChunk(fileChunksUploader, i, uploadId, path, projectId);
                if (!response.ok)
                    return {ok: false, id: uploadId, status: 'incomplete'}
                else if (i === nbChunks - 1) {//last chunk -> resource id as response
                    resourceId = await response.text();
                    console.log("resource ID => " + resourceId)
                }
            }
        }

        return {
            id: uploadId,
            resourceId: resourceId,
        }
    }


    async uploadFiles(files, projectId, collectionPath = null, acceptedFormat = null) {
        this.dispatchEvent(new CustomEvent(upload_events.start_upload_event, {detail: {nb_files: files.length}}));
        let uploadResults = [];
        for (let i = 0; i < files.length; i++) {//sequential upload promises to manage errors
            if (acceptedFormat == null || this.checkExtension(files[i], acceptedFormat)) {
                let path = (files[i]).webkitRelativePath.replace((files[i]).name, '');
                if (collectionPath)
                    path = collectionPath + '/' + path;
                let result = await this.uploadOrResumeFile(files[i], path, projectId);
                let uploadResult = await this.manageUploadFileResult(files[i], result);
                uploadResults.push(uploadResult);
            } else {
                uploadResults.push(
                    {
                        ok: false,
                        status: 'bad_format',
                        message: files[i].name + ' : mauvais format de fichier. Formats acceptés : ' + acceptedFormat.join(', '),
                        resource: files[i].name,
                    }
                )
            }
        }
        this.dispatchEvent(new CustomEvent(upload_events.end_upload_event, {detail: uploadResults}))
        return uploadResults;
    }

    async manageUploadFileResult(file, uploadResult) {
        // let result = null;
        // if (uploadResult.ok) {
            let result = {
                ok: true,
                name: file.name,
                id: uploadResult.id,
                resourceId: uploadResult.resourceId,
                message: "Fichier versé avec succès : " + file.name
            }
            this.dispatchEvent(new CustomEvent(upload_events.end_file_upload_event, {
                    'detail': result
                })
            );
        // } else {
        //     let errorMessage = "Une erreur est survenue lors du versement du fichier " + file.name;
        //     let result = {
        //         ok: false,
        //         status: 'error',
        //         message: errorMessage,
        //         name: file.name,
        //         hash: uploadResult.hash
        //     }
        //     this.dispatchEvent(new CustomEvent(upload_events.end_file_upload_event, {
        //             'detail': result
        //         })
        //     );
        // }
         return result;
    }

    checkExtension(file, acceptedFormats) {
        if (!acceptedFormats || acceptedFormats.length === 0)
            return true;

        let extension = file.name.split('.').pop();
        return acceptedFormats.indexOf(extension) >= 0;
    }


    addListener(listener) {
        if (this.listeners.indexOf(listener) < 0)
            this.listeners.push(listener);
    }

    dispatchEvent(event) {
        this.listeners.forEach(listener => listener.dispatchEvent(event))
    }


    getMediaAsBlob(mediaId) {
        let headers = {
            "X-Api-Key": this.apiKey
        }
        return fetch(this.endPoint + "download/" + mediaId, {headers}).then(response => response.blob())
    }

    getMediaURL(mediaId) {
        return this.endPoint + "download/" + mediaId
    }

}

export {JamaClient, upload_events}

