export default class JamaPermissions {

    constructor(permissionList, userPermissions, store) {
        this.permissionList = permissionList;//all jama available permissions
        this.userPermissions = userPermissions;//user permissions
        this.store = store;
    }

    canReadCollection(){
        return this.hasPermission('collection.read')
    }

    canCreateCollection(){
        return this.hasPermission('collection.create')
    }

    canUpdateCollection(){
        return this.hasPermission('collection.update')
    }

    canDeleteCollection(){
        return this.hasPermission('collection.delete')
    }

    canReadResource(){
        return this.hasPermission( 'resource.read')
    }

    canAddResource(){
        return this.hasPermission( 'resource.create')
    }

    canUpdateResource(){
        return this.hasPermission('resource.update')
    }

    canDeleteResource(){
        return this.hasPermission('resource.delete')
    }

    canFileUpload(){
        return this.hasPermission('file.upload')
            && this.hasPermission('resource.create')
            && this.hasPermission('file.create')
    }

    canFileDownload(){
         return this.hasPermission('file.download_source')
    }

    canEditMetadatasets(){
        return this.hasPermission('metadataset.create')
    }

    hasPermission(permissionLabel){
        try {
            let projectPermissions = this.userPermissions.find(pp => pp.project.id === Number(this.store.getters.currentProjectId)).role.permissions;
            let authorized = projectPermissions.find(p => p.label === permissionLabel)
            return authorized;
        }
        catch(e){
            console.log(e);
            console.log(this.userPermissions);
            return false;
        }
    }

    getPermissionList(){
        return this.permissionList;
    }

    getUserPermissions(){
        return this.userPermissions.find(pp => pp.project.id === this.store.getters.currentProjectId).role.permissions;
    }
}

