import RpcClient from "./RpcClient";


class JamaRpcClient extends RpcClient {

    constructor(endPoint = '', apiKey = '', rpcMethodPrefix = '') {
        super(endPoint, apiKey, rpcMethodPrefix);
    }

    /**
     * Create a new collection based on 'title' and parent_id
     *
     * Returns either the serialized new collection of null if parent does
     * not exist.
     *
     * Example output:
     *
     * ```
     * {
     *     "id": 3,
     *     "title": "paintings",
     *     "resources_count": 0,
     *     "children_count": 0,
     *     "descendants_count": 0,
     *     "descendants_resources_count": 0,
     *     "parent": null,
     *     "project_id": 1,
     *     "children": null,
     *     "metas": [],
     *     "public_access": false,
     *     "tags": [],
     * }
     * ```
     *
     * @param {String} title
     * @param {BigInteger} parent_id
     * @returns {Object}
     */
    async addCollection(title, parent_id) {
        return this._rpc("add_collection", [title, parent_id]);
    }


    /**
     * Will take a path such as '/photos/arts/paintings/'
     * and build the corresponding hierarchy of collections. The hierarchy
     * is returned as a list of serialized collections.
     *
     * Beware: Because the collections are serialized before their children,
     * all the children/descendants counts are set to 0.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         "id": 1,
     *         "title": "photos",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": null,
     *         "project_id": 1,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 2,
     *         "title": "arts",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 1,
     *         "project_id": 1,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 3,
     *         "title": "paintings",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 2,
     *         "project_id": 1,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     * ]
     * ```
     *
     * @param {String} path
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async addCollectionFromPath(path, project_id) {
        return this._rpc("add_collection_from_path", [path, project_id]);
    }


    /**
     * Add a meta value to a collection given their ids.
     *
     * If recursive is True, the meta will be added to all descendants,
     * collections and resources alike.
     *
     * @param {BigInteger} collection_id
     * @param {BigInteger} meta_id
     * @param {String} meta_value
     * @param {Boolean} recursive
     * @returns {BigInteger}
     */
    async addMetaToCollection(collection_id, meta_id, meta_value, recursive = false) {
        return this._rpc("add_meta_to_collection", [collection_id, meta_id, meta_value, recursive]);
    }


    /**
     * Add a meta value to a resource given their ids.
     *
     * @param {BigInteger} resource_id
     * @param {BigInteger} meta_id
     * @param {String} meta_value
     * @returns {BigInteger}
     */
    async addMetaToResource(resource_id, meta_id, meta_value) {
        return this._rpc("add_meta_to_resource", [resource_id, meta_id, meta_value]);
    }


    /**
     * Use such a dict for selection:
     * ```
     * {
     *     "include": {
     *         "resources_ids": [3493, 159]
     *         "collections_ids:" [20, 31]
     *     },
     *     "exclude": {
     *         "resources_ids": [12, 10, 15]
     *         "collections_ids:" [4, 254, 17]
     *     }
     * }
     * ```
     *
     * @param {BigInteger} from_collection_id
     * @param {Object} selection
     * @param {BigInteger} meta_id
     * @param {String} meta_value
     * @returns {Boolean}
     */
    async addMetaToSelection(from_collection_id, selection, meta_id, meta_value) {
        return this._rpc("add_meta_to_selection", [from_collection_id, selection, meta_id, meta_value]);
    }


    /**
     * Add a new metadata to metadata set.
     *
     * Set optional 'metadata_type_id'. Defaults to string type.
     *
     * @param {String} title
     * @param {BigInteger} metas_set_id
     * @param {BigInteger} metadata_type_id
     * @returns {BigInteger}
     */
    async addMetadata(title, metas_set_id, metadata_type_id = null) {
        return this._rpc("add_metadata", [title, metas_set_id, metadata_type_id]);
    }


    /**
     * Create new metadata set from title.
     *
     * @param {String} title
     * @param {BigInteger} project_id
     * @returns {BigInteger}
     */
    async addMetadataset(title, project_id) {
        return this._rpc("add_metadataset", [title, project_id]);
    }


    /**
     * Add a resource to a collection given ids.
     *
     * @param {BigInteger} resource_id
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async addResourceToCollection(resource_id, collection_id) {
        return this._rpc("add_resource_to_collection", [resource_id, collection_id]);
    }


    /**
     * Add tag to a collection based on tag uid and collection id.
     *
     * @param {String} tag_uid
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async addTagToCollection(tag_uid, collection_id) {
        return this._rpc("add_tag_to_collection", [tag_uid, collection_id]);
    }


    /**
     * Add tag to a resource based on tag uid and resource id.
     *
     * @param {String} tag_uid
     * @param {BigInteger} resource_id
     * @returns {Boolean}
     */
    async addTagToResource(tag_uid, resource_id) {
        return this._rpc("add_tag_to_resource", [tag_uid, resource_id]);
    }


    /**
     * Performs a complex search using terms such as 'contains', 'is', 'does_not_contain'.
     *
     * Multiple conditions can be added.
     *
     * Example input:
     *
     * ```
     * [
     *     {"property": "title", "term": "contains", "value": "cherbourg"},
     *     {"meta": 123, "term": "is", "value": "35mm"},
     *     {"exclude_meta": 145},
     *     {"tags": ["PAINTINGS", "PHOTOS"]}
     *     {"exclude_tags": ["DRAWINGS"]}
     * ]
     * ```
     *
     * Example output:
     *
     * ```
     * {
     *     "collections": [],
     *     "resources": [
     *         {
     *         "id": 1,
     *         "title": "Cherbourg by night",
     *         "original_name": "cherbourg_by_night.jpg",
     *         "type": "image/jpeg",
     *         "hash": "0dd93a59aeaccfb6d35b1ff5a49bde1196aa90dfef02892f9aa2ef4087d8738e",
     *         "metas": null,
     *         "urls": [],
     *         "tags": [],
     *         }
     *     ]
     * }
     * ```
     *
     * @param {Array} search_terms
     * @param {BigInteger} project_id
     * @param {Boolean} include_metas
     * @param {BigInteger} collection_id
     * @param {BigInteger} limit_from
     * @param {BigInteger} limit_to
     * @param {String} order_by
     * @returns {Object}
     */
    async advancedSearch(search_terms, project_id, include_metas = false, collection_id = null, limit_from = 0, limit_to = 2000, order_by = "title") {
        return this._rpc("advanced_search", [search_terms, project_id, include_metas, collection_id, limit_from, limit_to, order_by]);
    }


    /**
     * Return terms conditions to be used in advanced search.
     *
     * Example output:
     *
     * ```
     * [
     *     "is",
     *     "contains",
     *     "does_not_contain"
     * ]
     * ```
     *
     * @returns {Array}
     */
    async advancedSearchTerms() {
        return this._rpc("advanced_search_terms", []);
    }


    /**
     * Get ancestors from collection id as a list of serialized collections.
     *
     * If 'include_self' is true, will add the current collection at the begining.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         "id": 1,
     *         "title": "photos",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": null,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 2,
     *         "title": "arts",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 1,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 3,
     *         "title": "paintings",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 2,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     * ]
     * ```
     *
     * @param {BigInteger} collection_id
     * @param {Boolean} include_self
     * @returns {Array}
     */
    async ancestorsFromCollection(collection_id, include_self = false) {
        return this._rpc("ancestors_from_collection", [collection_id, include_self]);
    }


    /**
     * Get ancestors from resource id as a list of serialized collections.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         "id": 1,
     *         "title": "photos",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": null,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 2,
     *         "title": "arts",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 1,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     *     {
     *         "id": 3,
     *         "title": "paintings",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": 2,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *     },
     * ]
     * ```
     *
     * @param {BigInteger} resource_id
     * @returns {Array}
     */
    async ancestorsFromResource(resource_id) {
        return this._rpc("ancestors_from_resource", [resource_id]);
    }


    /**
     * Tries to determine skew angle of image with text.
     *
     * @param {BigInteger} resource_id
     * @returns {Number}
     */
    async autoFindRotateAngle(resource_id) {
        return this._rpc("auto_find_rotate_angle", [resource_id]);
    }


    /**
     * Change the value of a meta for a collection.
     *
     * @param {BigInteger} meta_value_id
     * @param {String} meta_value
     * @returns {Boolean}
     */
    async changeCollectionMetaValue(meta_value_id, meta_value) {
        return this._rpc("change_collection_meta_value", [meta_value_id, meta_value]);
    }


    /**
     * Change the value of a meta for a resource
     *
     * @param {BigInteger} meta_value_id
     * @param {String} meta_value
     * @returns {Boolean}
     */
    async changeResourceMetaValue(meta_value_id, meta_value) {
        return this._rpc("change_resource_meta_value", [meta_value_id, meta_value]);
    }


    /**
     * Get a particular collection given its id.
     *
     * Example output:
     *
     * ```
     * {
     *     "id": 2,
     *     "title": "art works",
     *     "resources_count": 23,
     *     "children_count": 5,
     *     "parent": 1,
     *     "children": None,
     *     "metas": [],
     *     "public_access": False,
     *     "tags": [],
     * }
     * ```
     *
     * @param {BigInteger} collection_id
     * @returns {Object}
     */
    async collection(collection_id) {
        return this._rpc("collection", [collection_id]);
    }


    /**
     * Get infos from given collection:
     *
     * - number of descendants
     * - number of descendant resources
     * - number of resources
     * - number of children collections
     *
     * @param {BigInteger} collection_id
     * @returns {Object}
     */
    async collectionStats(collection_id) {
        return this._rpc("collection_stats", [collection_id]);
    }


    /**
     * Return the user's collections under the parent collection
     * specified by 'parent_id'. If 'recursive' is true, will
     * return all the descendants recursively in the 'children' key.
     * If recursive is false, 'children' is null.
     *
     * Special case:
     *
     * If flat_list is True, collections are returned as a flat list and parent_id is effectively IGNORED.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         "id": 2,
     *         "title": "art works",
     *         "resources_count": 23,
     *         "children_count": 5,
     *         "descendants_count": 12,
     *         "descendants_resources_count": 58,
     *         "parent": 1,
     *         "children": None,
     *         "metas": [],
     *         "public_access": False,
     *         "tags": [],
     *     }
     * ]
     * ```
     *
     * @param {BigInteger} parent_id
     * @param {Boolean} recursive
     * @param {BigInteger} limit_from
     * @param {BigInteger} limit_to
     * @param {Boolean} flat_list
     * @param {Boolean} only_published
     * @param {String} order_by
     * @param {Boolean} only_deleted_items
     * @returns {Array}
     */
    async collections(parent_id, recursive = false, limit_from = 0, limit_to = 2000, flat_list = false, only_published = false, order_by = "title", only_deleted_items = false) {
        return this._rpc("collections", [parent_id, recursive, limit_from, limit_to, flat_list, only_published, order_by, only_deleted_items]);
    }


    /**
     * Create a new project.
     *
     * Requires superuser.
     *
     * @param {String} project_label
     * @param {String} project_description
     * @returns {Object}
     */
    async createProject(project_label, project_description) {
        return this._rpc("create_project", [project_label, project_description]);
    }


    /**
     * Deactivate access to the RPC API for the given user name and API key.
     * Only the access (API key) is removed, not the user.
     *
     * Requires superuser.
     *
     * @param {String} user_name
     * @param {String} api_key
     * @returns {Boolean}
     */
    async deactivateRpcAccess(user_name, api_key) {
        return this._rpc("deactivate_rpc_access", [user_name, api_key]);
    }


    /**
     * Delete collection given its id.
     *
     * Collection MUST be empty of any content (no children collections and no resources),
     * unless the 'recursive'parameter is set to True, in which case ALL descendants will be
     * deleted.
     *
     * @param {BigInteger} collection_id
     * @param {Boolean} recursive
     * @returns {Object}
     */
    async deleteCollection(collection_id, recursive = false) {
        return this._rpc("delete_collection", [collection_id, recursive]);
    }


    /**
     * Delete metadata based on its id.
     *
     * @param {BigInteger} metadata_id
     * @returns {Boolean}
     */
    async deleteMetadata(metadata_id) {
        return this._rpc("delete_metadata", [metadata_id]);
    }


    /**
     * Delete metadata set based on its id. Optional recursive
     * call.
     *
     * @param {BigInteger} metadataset_id
     * @param {Boolean} recursive
     * @returns {Object}
     */
    async deleteMetadataset(metadataset_id, recursive = false) {
        return this._rpc("delete_metadataset", [metadataset_id, recursive]);
    }


    /**
     * Delete a property from the project.
     *
     * property_key is NOT case sensitive, ie. "ProPertY" is the same as "pRoperTy" or "property".
     *
     * @param {BigInteger} project_id
     * @param {String} property_key
     * @returns {Boolean}
     */
    async deleteProjectProperty(project_id, property_key) {
        return this._rpc("delete_project_property", [project_id, property_key]);
    }


    /**
     * Permanently (soft) delete a resource given its id.
     *
     * @param {BigInteger} resource_id
     * @returns {Boolean}
     */
    async deleteResource(resource_id) {
        return this._rpc("delete_resource", [resource_id]);
    }


    /**
     * Delete role within given project.
     *
     * Requires superuser
     *
     * @param {BigInteger} project_id
     * @param {String} role_label
     * @returns {Boolean}
     */
    async deleteRole(project_id, role_label) {
        return this._rpc("delete_role", [project_id, role_label]);
    }


    /**
     * Remove (delete) a tag based on its uid.
     *
     * Beware: This will remove ALL associations with the tag.
     *
     * @param {String} uid
     * @returns {Boolean}
     */
    async deleteTag(uid) {
        return this._rpc("delete_tag", [uid]);
    }


    /**
     * Test current user for given permission.
     *
     * @param {BigInteger} project_id
     * @param {String} permission
     * @returns {Boolean}
     */
    async hasPermission(project_id, permission) {
        return this._rpc("has_permission", [project_id, permission]);
    }


    /**
     * Lists all available permissions in the application:
     *
     * ```
     * [
     *     {'id': 1, 'label': 'collection.create'},
     *     {'id': 2, 'label': 'collection.read'},
     *     {'id': 3, 'label': 'collection.update'},
     *     {'id': 4, 'label': 'collection.delete'},
     *     {'id': 5, 'label': 'resource.create'},
     *     {'id': 6, 'label': 'resource.read'},
     *     {'id': 7, 'label': 'resource.update'},
     *     {'id': 8, 'label': 'resource.delete'},
     *     {'id': 9, 'label': 'metadata.create'},
     *     {'id': 10, 'label': 'metadata.read'},
     *     {'id': 11, 'label': 'metadata.update'},
     *     {'id': 12, 'label': 'metadata.delete'},
     *     {'id': 13, 'label': 'metadataset.create'},
     *     {'id': 14, 'label': 'metadataset.read'},
     *     {'id': 15, 'label': 'metadataset.update'},
     *     {'id': 16, 'label': 'metadataset.delete'},
     *     {'id': 17, 'label': 'file.create'},
     *     {'id': 18, 'label': 'file.read'},
     *     {'id': 19, 'label': 'file.update'},
     *     {'id': 20, 'label': 'file.delete'},
     *     {'id': 21, 'label': 'tag.create'},
     *     {'id': 22, 'label': 'tag.read'},
     *     {'id': 23, 'label': 'tag.update'},
     *     {'id': 24, 'label': 'tag.delete'},
     *     {'id': 25, 'label': 'file.download_source'}
     * ]
     * ```
     *
     * @returns {Array}
     */
    async listPermissions() {
        return this._rpc("list_permissions", []);
    }


    /**
     * Fetch all roles defined in the project, no matter the user.
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async listRoles(project_id) {
        return this._rpc("list_roles", [project_id]);
    }


    /**
     * Count metadata usage.
     *
     * @param {BigInteger} metadata_id
     * @param {BigInteger} collection_id
     * @returns {Object}
     */
    async metaCount(metadata_id, collection_id) {
        return this._rpc("meta_count", [metadata_id, collection_id]);
    }


    /**
     * Get one particular metadata given its id.
     *
     * Example output:
     *
     * ```
     * {
     *     "id": 2,
     *     "title": "ICC_Profile:GrayTRC",
     *     "set_id": 1,
     *     "set_title": "exif metas",
     *     "rank": 1,
     *     "project_id": 1,
     * }
     * ```
     *
     * @param {BigInteger} metadata_id
     * @returns {Object}
     */
    async metadata(metadata_id) {
        return this._rpc("metadata", [metadata_id]);
    }


    /**
     * Get all metadatas given a metadata set id.
     *
     * Metadatas MAY be ordered with the rank attribute.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         "id": 1,
     *         "title": "PNG:ProfileName",
     *         "set_id": 1,
     *         "set_title": "exif metas",
     *         "rank": 0,
     *         "project_id": 1,
     *     },
     *     {
     *         "id": 2,
     *         "title": "ICC_Profile:GrayTRC",
     *         "set_id": 1,
     *         "set_title": "exif metas",
     *         "rank": 1,
     *         "project_id": 1,
     *     }
     * ]
     * ```
     *
     * @param {BigInteger} metadata_set_id
     * @returns {Array}
     */
    async metadatas(metadata_set_id) {
        return this._rpc("metadatas", [metadata_set_id]);
    }


    /**
     * Get the list of all the project's metadata sets.
     * For each metadatas set, the number of metadatas is given in metas_count.
     *
     * Example output:
     *
     * ```
     * [
     *     {"id": 1, "title": "exif metas", "project_id": 1, "metas_count": 23},
     *     {"id": 2, "title": "dublin core", "project_id": 1, "metas_count": 17}
     * ]
     * ```
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async metadatasets(project_id) {
        return this._rpc("metadatasets", [project_id]);
    }


    /**
     * Get a list of available data types
     *
     * Example output:
     *
     * ```
     * [
     *     {"id": 1, "title": "text"},
     *     {"id": 2, "title": "numeric"},
     * ]
     * ```
     *
     * @returns {Array}
     */
    async metadatatypes() {
        return this._rpc("metadatatypes", []);
    }


    /**
     * Move a collection from a parent to another.
     *
     * Will raise ServiceException in the following cases:
     *
     * - 'child_collection_id' and 'parent_collection_id' are equal
     * - parent collection does not exist
     * - parent collection is a descendant of child collection
     *
     * @param {BigInteger} child_collection_id
     * @param {BigInteger} parent_collection_id
     * @returns {Boolean}
     */
    async moveCollection(child_collection_id, parent_collection_id) {
        return this._rpc("move_collection", [child_collection_id, parent_collection_id]);
    }


    /**
     * Move items (collections or resources) from one Collection to another
     *
     * @param {BigInteger} from_collection_id
     * @param {BigInteger} to_collection_id
     * @param {Array} collections_ids
     * @param {Array} resources_ids
     * @returns {Object}
     */
    async moveItems(from_collection_id, to_collection_id, collections_ids, resources_ids) {
        return this._rpc("move_items", [from_collection_id, to_collection_id, collections_ids, resources_ids]);
    }


    /**
     * Will mass move items (resources AND collections) based on parent collection and destination collection
     *
     * Use such an object for inclusion/exclusion:
     *
     * ```
     * {
     *     "include": {
     *         "resources_ids": [3493, 159]
     *         "collections_ids:" [20, 31]
     *     },
     *     "exclude": {
     *         "resources_ids": [12, 10, 15]
     *         "collections_ids:" [4, 254, 17]
     *     }
     * }
     * ```
     *
     * @param {BigInteger} from_collection_id
     * @param {Object} selection
     * @param {BigInteger} to_collection_id
     * @returns {Object}
     */
    async moveSelection(from_collection_id, selection, to_collection_id) {
        return this._rpc("move_selection", [from_collection_id, selection, to_collection_id]);
    }


    /**
     * Rotate and crop an image. The resulting image then replaces the
     * original in the current resource.
     *
     * Will return the resource upon success. Throws a ServiceException
     * otherwise.
     *
     * @param {BigInteger} resource_id
     * @param {Number} rotation
     * @param {BigInteger} top_crop
     * @param {BigInteger} right_crop
     * @param {BigInteger} bottom_crop
     * @param {BigInteger} left_crop
     * @returns {Object}
     */
    async pictureRotateCrop(resource_id, rotation = 0, top_crop = 0, right_crop = 0, bottom_crop = 0, left_crop = 0) {
        return this._rpc("picture_rotate_crop", [resource_id, rotation, top_crop, right_crop, bottom_crop, left_crop]);
    }


    /**
     * This is a test method to ensure the server-client communication works.
     * Will return "pong [name authenticated of user]"
     *
     * Example output:
     *
     * ```
     * pong john
     * ```
     *
     * @returns {String}
     */
    async ping() {
        return this._rpc("ping", []);
    }


    /**
     * Alias to advanced_search.
     *
     * @param {Array} search_terms
     * @param {BigInteger} project_id
     * @param {Boolean} include_metas
     * @param {BigInteger} collection_id
     * @param {BigInteger} limit_from
     * @param {BigInteger} limit_to
     * @param {String} order_by
     * @returns {Object}
     */
    async projectItems(search_terms, project_id, include_metas = false, collection_id = null, limit_from = 0, limit_to = 2000, order_by = "title", public_access = null) {
        return this._rpc("project_items", [search_terms, project_id, include_metas, collection_id, limit_from, limit_to, order_by, public_access]);
    }


    /**
     * Get ALL properties from a project.
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async projectProperties(project_id) {
        return this._rpc("project_properties", [project_id]);
    }


    /**
     * Get a property value from the project.
     *
     * property_key is NOT case sensitive, ie. "ProPertY" is the same as "pRoperTy" or "property".
     *
     * Will raise an exception if property does not exist.
     *
     * @param {BigInteger} project_id
     * @param {String} property_key
     * @returns {Object}
     */
    async projectProperty(project_id, property_key) {
        return this._rpc("project_property", [project_id, property_key]);
    }


    /**
     * Get infos from given project:
     *
     * - id of project collection root
     * - number of descendants
     * - number of descendant resources
     * - number of resources
     * - number of children collections
     *
     * @param {BigInteger} project_id
     * @returns {Object}
     */
    async projectStats(project_id) {
        return this._rpc("project_stats", [project_id]);
    }


    /**
     * Get all rights for the current user.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *         'project': {'id': 7, 'label': 'john doe main project'},
     *         'role': {'id': 7, 'label': 'admin', 'permissions': [{"id": 1, "label": "do_anything"}]},
     *         'user': 'john doe'
     *     }
     * ]
     * ```
     *
     * @returns {Array}
     */
    async projectsUserPermissions() {
        return this._rpc("projects_user_permissions", []);
    }


    /**
     * Get public collections
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async publicCollections(project_id) {
        return this._rpc("public_collections", [project_id]);
    }


    /**
     * Mark a collection as public
     *
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async publishCollection(collection_id) {
        return this._rpc("publish_collection", [collection_id]);
    }


    /**
     * Gets deleted elements:
     *
     * - object type
     * - label
     * - id
     * - deleted_at
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async recycleBin(project_id) {
        return this._rpc("recycle_bin", [project_id]);
    }


    /**
     * Remove a meta value from a collection given their ids.
     *
     * @param {BigInteger} collection_id
     * @param {BigInteger} meta_value_id
     * @param {Boolean} recursive
     * @returns {Boolean}
     */
    async removeMetaValueFromCollection(collection_id, meta_value_id, recursive = false) {
        return this._rpc("remove_meta_value_from_collection", [collection_id, meta_value_id, recursive]);
    }


    /**
     * Remove a meta_value from a resource given their ids.
     *
     * @param {BigInteger} resource_id
     * @param {BigInteger} meta_value_id
     * @returns {Boolean}
     */
    async removeMetaValueFromResource(resource_id, meta_value_id) {
        return this._rpc("remove_meta_value_from_resource", [resource_id, meta_value_id]);
    }


    /**
     * Use such a dict for selection:
     * ```
     * {
     *     "include": {
     *         "resources_ids": [3493, 159]
     *         "collections_ids:" [20, 31]
     *     },
     *     "exclude": {
     *         "resources_ids": [12, 10, 15]
     *         "collections_ids:" [4, 254, 17]
     *     }
     * }
     * ```
     *
     * @param {BigInteger} from_collection_id
     * @param {Object} selection
     * @param {BigInteger} meta_value_id
     * @returns {Boolean}
     */
    async removeMetaValueFromSelection(from_collection_id, selection, meta_value_id) {
        return this._rpc("remove_meta_value_from_selection", [from_collection_id, selection, meta_value_id]);
    }


    /**
     * Remove a resource from a collection given ids.
     *
     * @param {BigInteger} resource_id
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async removeResourceFromCollection(resource_id, collection_id) {
        return this._rpc("remove_resource_from_collection", [resource_id, collection_id]);
    }


    /**
     * Will mass remove items (resources AND collections) based on parent collection
     *
     * Use such an object for inclusion/exclusion:
     *
     * ```
     * {
     *     "include": {
     *         "resources_ids": [3493, 159]
     *         "collections_ids:" [20, 31]
     *     },
     *     "exclude": {
     *         "resources_ids": [12, 10, 15]
     *         "collections_ids:" [4, 254, 17]
     *     }
     * }
     * ```
     *
     * deleteCollection (with recursion) and deleteResource are used under the hood.
     *
     * The parent collection is left as-is.
     *
     * @param {BigInteger} parent_collection_id
     * @param {Object} selection
     * @returns {Boolean}
     */
    async removeSelection(parent_collection_id, selection) {
        return this._rpc("remove_selection", [parent_collection_id, selection]);
    }


    /**
     * Remove tag from a collection based on tag uid and collection id.
     *
     * @param {String} tag_uid
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async removeTagFromCollection(tag_uid, collection_id) {
        return this._rpc("remove_tag_from_collection", [tag_uid, collection_id]);
    }


    /**
     * Remove tag from a resource based on tag uid and resource id.
     *
     * @param {String} tag_uid
     * @param {BigInteger} resource_id
     * @returns {Boolean}
     */
    async removeTagFromResource(tag_uid, resource_id) {
        return this._rpc("remove_tag_from_resource", [tag_uid, resource_id]);
    }


    /**
     * Rename a collection (ie. change its title).
     *
     * @param {BigInteger} collection_id
     * @param {String} title
     * @returns {Boolean}
     */
    async renameCollection(collection_id, title) {
        return this._rpc("rename_collection", [collection_id, title]);
    }


    /**
     * Rename a metadata (ie. change its title).
     *
     * @param {BigInteger} meta_id
     * @param {String} title
     * @returns {Boolean}
     */
    async renameMeta(meta_id, title) {
        return this._rpc("rename_meta", [meta_id, title]);
    }


    /**
     * Rename a resource (ie. change its title).
     *
     * @param {BigInteger} resource_id
     * @param {String} title
     * @returns {Boolean}
     */
    async renameResource(resource_id, title) {
        return this._rpc("rename_resource", [resource_id, title]);
    }


    /**
     * Replace a file by another using two existing resources.
     *
     * The two resources are expected to be of File type. Then the
     * following operations are performed:
     *
     * - metas from the "ExifTool" set are removed from the destination resource instance
     * - metas from the "ExifTool" set are transfered from the source resource instance to the destination resource instance
     * - the destination resource instance gets the file hash from the source resource instance
     * - the source resource instance is (hard) deleted
     * - the destination resource instance is saved
     *
     * Such that all title/metas/tags/collections of the destination resource instance are untouched,
     * excluding exif metas that are transfered from the source.
     *
     * @param {BigInteger} from_resource_id
     * @param {BigInteger} to_resource_id
     * @returns {Boolean}
     */
    async replaceFile(from_resource_id, to_resource_id) {
        return this._rpc("replace_file", [from_resource_id, to_resource_id]);
    }


    /**
     * Get a resource given its id.
     *
     * Example output (file resource):
     *
     * ```
     * {
     *     "id": 1,
     *     "title": "letter",
     *     "original_name": "letter.txt",
     *     "type": "text/plain",
     *     "hash": "0dd93a59aeaccfb6d35b1ff5a49bde1196aa90dfef02892f9aa2ef4087d8738e",
     *     "metas": null,
     *     "urls": [],
     *     "tags": [],
     * }
     * ```
     *
     * @param {BigInteger} resource_id
     * @returns {Object}
     */
    async resource(resource_id) {
        return this._rpc("resource", [resource_id]);
    }


    /**
     * Get all resources from a collection.
     *
     * If 'include_metas' is true, will return the resources metadatas.
     * If 'include_metas' is false, 'metas' will be null.
     *
     * Different resources types may have different object keys. The bare
     * minimum is 'id', 'title' and 'tags'.
     *
     * Example output (file resource):
     *
     * ```
     * [
     *     {
     *         "id": 1,
     *         "title": "letter",
     *         "original_name": "letter.txt",
     *         "type": "text/plain",
     *         "hash": "0dd93a59aeaccfb6d35b1ff5a49bde1196aa90dfef02892f9aa2ef4087d8738e",
     *         "metas": null,
     *         "urls": [],
     *         "tags": [],
     *     }
     * ]
     * ```
     *
     * @param {BigInteger} collection_id
     * @param {Boolean} include_metas
     * @param {BigInteger} limit_from
     * @param {BigInteger} limit_to
     * @param {String} order_by
     * @param {Boolean} only_deleted_items
     * @param {Array} only_tags
     * @returns {Array}
     */
    async resources(collection_id, include_metas = false, limit_from = 0, limit_to = 2000, order_by = "title", only_deleted_items = false, only_tags = null) {
        return this._rpc("resources", [collection_id, include_metas, limit_from, limit_to, order_by, only_deleted_items, only_tags]);
    }


    /**
     * Restore a deleted collection from the recycle bin
     *
     * @param {BigInteger} collection_id
     * @param {BigInteger} destination_collection_id
     * @returns {Boolean}
     */
    async restoreCollection(collection_id, destination_collection_id) {
        return this._rpc("restore_collection", [collection_id, destination_collection_id]);
    }


    /**
     * Restore a deleted resource from the recycle bin
     *
     * @param {BigInteger} resource_id
     * @param {BigInteger} destination_collection_id
     * @returns {Boolean}
     */
    async restoreResource(resource_id, destination_collection_id) {
        return this._rpc("restore_resource", [resource_id, destination_collection_id]);
    }


    /**
     * Set/unset the collection as a OAI-PMH record. The creation date
     * will be used in OAI-PMH requests.
     *
     * @param {BigInteger} collection_id
     * @param {Boolean} is_oai_record
     * @returns {Boolean}
     */
    async setIsOaiRecord(collection_id, is_oai_record = true) {
        return this._rpc("set_is_oai_record", [collection_id, is_oai_record]);
    }


    /**
     * Sets all metas for a unique metadata set.
     *
     * Metas is a list of metadata id => metadata value dictionaries.
     *
     * All metas must share the same metadata set.
     *
     * If recursive is True, the meta will be added to all descendants,
     * collections and resources alike.
     *
     * @param {BigInteger} collection_id
     * @param {Array} metas
     * @param {Boolean} recursive
     * @param {Boolean} async_mode
     * @returns {Boolean}
     */
    async setMetasToCollection(collection_id, metas, recursive = false, async_mode = false) {
        return this._rpc("set_metas_to_collection", [collection_id, metas, recursive, async_mode]);
    }


    /**
     * Sets all metas for a unique metadata set.
     *
     * Metas is a list of metadata id => metadata value dictionaries.
     *
     * All metas must share the same metadata set.
     *
     * @param {BigInteger} resource_id
     * @param {Array} metas
     * @returns {Boolean}
     */
    async setMetasToResource(resource_id, metas) {
        return this._rpc("set_metas_to_resource", [resource_id, metas]);
    }


    /**
     * Set a property value to the project.
     *
     * property_key is NOT case sensitive, ie. "ProPertY" is the same as "pRoperTy" or "property".
     *
     * @param {BigInteger} project_id
     * @param {String} property_key
     * @param {Object} property_value
     * @returns {Object}
     */
    async setProjectProperty(project_id, property_key, property_value) {
        return this._rpc("set_project_property", [project_id, property_key, property_value]);
    }


    /**
     * Choose a Resource that is the best representation of a collection.
     * Typical use case: set a miniature for a collection.
     *
     * The Resource does not have to be contained in the collection.
     *
     * Resource id may be set set to None/Null.
     *
     * @param {BigInteger} collection_id
     * @param {BigInteger} resource_id
     * @returns {Boolean}
     */
    async setRepresentativeResource(collection_id, resource_id = null) {
        return this._rpc("set_representative_resource", [collection_id, resource_id]);
    }


    /**
     * Create or update a role on a project, with the given permissions.
     *
     * Requires superuser.
     *
     * @param {BigInteger} project_id
     * @param {String} role_label
     * @param {Array} permissions
     * @returns {Object}
     */
    async setRole(project_id, role_label, permissions) {
        return this._rpc("set_role", [project_id, role_label, permissions]);
    }


    /**
     * Get or create a Tag by uid (unique identifier). 'label' is an optional human-readable name.
     *
     * Example output:
     *
     * ```
     * {
     *     "id": 1,
     *     "uid": "PAINTINGS",
     *     "label": "peintures",
     *     "ark": null,
     * }
     * ```
     *
     * @param {String} uid
     * @param {BigInteger} project_id
     * @param {String} label
     * @param {String} ark
     * @returns {Object}
     */
    async setTag(uid, project_id, label = null, ark = null) {
        return this._rpc("set_tag", [uid, project_id, label, ark]);
    }


    /**
     * Performs a simple search on resources and collections, based on their titles.
     *
     * Example output:
     *
     * ```
     * {
     *     "collections": [
     *         {
     *         "id": 1,
     *         "title": "photos",
     *         "resources_count": 0,
     *         "children_count": 0,
     *         "descendants_count": 0,
     *         "descendants_resources_count": 0,
     *         "parent": null,
     *         "children": null,
     *         "metas": [],
     *         "public_access": false,
     *         "tags": [],
     *         }
     *     ],
     *     "resources": [
     *         {
     *         "id": 1,
     *         "title": "letter",
     *         "original_name": "letter.txt",
     *         "type": "text/plain",
     *         "hash": "0dd93a59aeaccfb6d35b1ff5a49bde1196aa90dfef02892f9aa2ef4087d8738e",
     *         "metas": null,
     *         "urls": [],
     *         "tags": [],
     *         }
     *     ]
     * }
     * ```
     *
     * @param {String} query
     * @param {BigInteger} project_id
     * @param {BigInteger} limit_from
     * @param {BigInteger} limit_to
     * @param {String} order_by
     * @returns {Object}
     */
    async simpleSearch(query, project_id, limit_from = 0, limit_to = 2000, order_by = "title") {
        return this._rpc("simple_search", [query, project_id, limit_from, limit_to, order_by]);
    }


    /**
     * Get a list of all supported file type, complete with their mimes.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *     "mime": "image/jpeg",
     *     "extensions": [".jpg", ".jpeg"],
     *     "iiif_support": true,
     *     }
     * ]
     * ```
     *
     * @returns {Array}
     */
    async supportedFileTypes() {
        return this._rpc("supported_file_types", []);
    }


    /**
     * Returns all tags available in the project.
     *
     * Example output:
     *
     * ```
     * [
     *     {
     *     "id": 1,
     *     "uid": "PAINTINGS",
     *     "label": "peintures",
     *     "ark": null,
     *     },
     *     {
     *     "id": 2,
     *     "uid": "PHOTOS",
     *     "label": "photos",
     *     "ark": null,
     *     }
     * ]
     * ```
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async tags(project_id) {
        return this._rpc("tags", [project_id]);
    }


    /**
     * Mark a collection as private
     *
     * @param {BigInteger} collection_id
     * @returns {Boolean}
     */
    async unpublishCollection(collection_id) {
        return this._rpc("unpublish_collection", [collection_id]);
    }


    /**
     * Get information for an upload based on the file hash.
     *
     * Example output:
     *
     * ```
     * {
     *     "status": "not available",
     *     "id": null,
     *     "available_chunks":[]
     * }
     * ```
     *
     * "status" being one of "not available", "available" or "incomplete"
     *
     * @param {String} sha256_hash
     * @param {BigInteger} project_id
     * @returns {Object}
     */
    async uploadInfos(sha256_hash, project_id) {
        return this._rpc("upload_infos", [sha256_hash, project_id]);
    }

    /**
     * Returns list of user tasks. Each task being serialized like so:
     *
     * ```
     * {
     *     "object_type": "task",
     *     "id": user_task_instance.id,
     *     "description": user_task_instance.description,
     *     "created_at": user_task_instance.created_at.isoformat(),
     *     "started_at": user_task_instance.started_at.isoformat(),
     *     "finished_at": user_task_instance.finished_at.isoformat(),
     *     "failed_at": user_task_instance.failed_at.isoformat(),
     * }
     * ```
     *
     * @param {BigInteger} project_id
     * @returns {Array}
     */
    async userTasksStatus(project_id = null) {
        return this._rpc("user_tasks_status", [project_id]);
    }
}

export default JamaRpcClient
