export default class VuetifyUtils {

    static prepareTreeViewCollections(cols, ancestors) {
        let prepared = [];
        let ancestorsIds = VuetifyUtils.prepareTreeViewAncestorsIds(ancestors)||[];
        for(let i = 0 ; i < cols.length; i++) {
            let col = {
                title: cols[i].title,
                id: cols[i].id,
                locked: ancestorsIds.indexOf(cols[i].id) >= 0
            }
            if(cols[i].children_count > 0 /*&& !VuetifyUtils.isAncestorId(cols[i].id, ancestors)*/)
                col.children = []
            prepared.push(col)
        }
        return prepared;
    }

    static prepareTreeViewAncestorsIds(ancestors){
        let prepared = [];
        if(!ancestors)
            return prepared;
        for(let i = 0 ; i < ancestors.length; i++) {
            let leaf = ancestors[i].slice(-1)[0];
            prepared.push(leaf.id)
        }
        return prepared
    }

    static isAncestorId(id, ancestors){
        if(!ancestors)
            return false;
        for(let i = 0; i < ancestors.length; i++){
            for(let j = 0; j < ancestors[i].length; j++){
                if((ancestors[i][j]).id === id)
                    return true;
            }
        }
        return false;
    }
}
