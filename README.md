# jama-vue-client

## Project setup
```
yarn install
```

## Jama Service Auth

create a `.env.local` file for Jama service connection settings, for example :

```
VUE_APP_JAMA_END_POINT=http://localhost:8000/rpc/
VUE_APP_JAMA_API_KEY=myJamaApiKey
```



### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production

Set app title (nav bar label) in `.env.local` file & base url if needed:

```
VUE_APP_TITLE=My app title
VUE_APP_URL_PATH=/my-base-url/
```
then build with 
```
yarn build
```


**note :** `dist/assets` folder must be served at `https://mysite.mydomain/myroute/assets`


### Lints and fixes files
```
yarn lint
```

# Usage

```
yarn add @chauveau/vue-jama
```

Your ~/.npmrc must contains :

```
@chauveau:registry=https://git.unicaen.fr/api/v4/packages/npm/
```

## Import

```
import {VueJamaApp} from "@chauveau/vue-jama";

or

import {VueJamaDialog} from "@chauveau/vue-jama";


```

## Build lib

```
yarn build-lib
```

## Publish

```
npm publish
```

## reference the component via `<vue-jama-picker>` or `<vue-jama>` tag as following

```

const options = {
                  endPoint: 'jama-endpoint',
                  apiKey: 'jama api key', // optionnal
                  appTitle: 'app title', // optionnal
                  defaultProjectId: collectionId, // optionnal
                  defaultCollectionId: collectionId,// optionnal

};

            
<vue-jama-picker
        :options="options"
        @resource-selected="resourceSelectedCallBack"
></vue-jama-dialog>
          
OR

<vue-jama :options="options"></vue-jama>
```



## Add plugin component and custom metadata component in options

```
import HelloWorldPlugin from "../components/plugins/HelloWorldPlugin";
import MyCustomMetadataEditor from "../components/MyCustomMetadataEditor";

const options = {
                  endPoint: 'jama-endpoint',
                  apiKey: 'jama api key', // optionnal
                  appTitle: 'app title', // optionnal
                  defaultProjectId: collectionId, // optionnal
                  defaultCollectionId: collectionId,// optionnal
                  
                  metadataComponent: MyCustomMetadataEditor,
                  plugins : [
                    {
                        component: HelloWorldPlugin,
                        title: 'My plugin title',
                        icon: 'mdi-eye'
                     }
                   ]
                };
                

```

### Opentheso concepts (as tags) option

```
[...]
conceptsConfig: {  // optionnal
    endPoint : 'https://atlas-pp.certic.unicaen.fr/opentheso2/api/',
    thesaurusId : 'th1',
    thesaurusLang : 'fr'
}
[...]
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

